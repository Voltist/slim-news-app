package com.artifxl.slim

import android.content.Context
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup

import kotlinx.android.synthetic.main.activity_main_2.*
import kotlinx.android.synthetic.main.fragment_main_2.view.*
import org.json.JSONObject

class Main_2 : AppCompatActivity() {

    /**
     * The [android.support.v4.view.PagerAdapter] that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * [android.support.v4.app.FragmentStatePagerAdapter].
     */
    private var title_list = ArrayList<String>(0)
    private var publish_list = ArrayList<String>(0)

    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null
    var mApi : Api? = null

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main_2)

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.

        mApi = Api(this)

        mApi!!.get_headlines {response ->
            if (response == null) {

            } else if (response.getBoolean("success")) {
                val tmp_list = response.getJSONArray("data")
                for (i in 0..tmp_list.length()-1) {
                    val tmp_json = JSONObject(tmp_list[i].toString())
                    val title_string = tmp_json.getString("title")
                    val publish_info = tmp_json.getString("source") + " " + tmp_json.getString("time")
                    title_list.add(title_string)
                    publish_list.add(publish_info)
                }
                mSectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager)
            }
        }

        // Set up the ViewPager with the sections adapter.
        container.adapter = mSectionsPagerAdapter

    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main_2, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        if (id == R.id.action_settings) {
            return true
        }

        return super.onOptionsItemSelected(item)
    }


    /**
     * A [FragmentPagerAdapter] that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
        override fun getItem(position: Int): Fragment {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(title_list[position], publish_list[position], this@Main_2)
        }

        override fun getCount(): Int {
            // Show 3 total pages.
            return title_list.size
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    class PlaceholderFragment : Fragment() {

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                                  savedInstanceState: Bundle?): View? {
            val rootView = inflater.inflate(R.layout.fragment_main_2, container, false)
            val title = arguments!!.getString(ARG_TITLE)
            rootView.title.text = title
            rootView.publish_info.text = arguments?.getString(ARG_PUBLISH)

            mApi!!.get_content(title) { response ->
                if (response == null) {
                } else if (response.getBoolean("success")) {
                    if (response.getJSONArray("data").length() > 0) {
                        rootView.content.text = response.getJSONArray("data")[0].toString()
                    }
                }
            }

            return rootView
        }

        companion object {
            /**
             * The fragment argument representing the section number for this
             * fragment.
             */
            private val ARG_TITLE = "title"
            private val ARG_PUBLISH = "publisher"
            private val ARG_CONTENT = "content"

            private var mApi: Api? = null


            /**
             * Returns a new instance of this fragment for the given section
             * number.
             */
            fun newInstance(title: String, publish_info: String, context: Context): PlaceholderFragment {
                val fragment = PlaceholderFragment()
                val args = Bundle()
                mApi = Api(context)
                args.putString(ARG_TITLE, title)
                args.putString(ARG_PUBLISH, publish_info)

                fragment.arguments = args
                return fragment
            }
        }
    }
}
