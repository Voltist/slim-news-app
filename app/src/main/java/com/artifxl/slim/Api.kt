package com.artifxl.slim

import android.content.Context
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.*
import org.json.JSONObject
import java.nio.channels.CompletionHandler

class Api(context: Context) {
    val requestQueue = Volley.newRequestQueue(context)

    val base_url = "https://slim.artifxl.com/api"


    fun get_headlines(callback: (JSONObject?) -> Unit) {
        val extension = "headlines"
        // Formulate the request and handle the response.
        val jsonObj = JsonObjectRequest(Request.Method.POST, base_url+extension, null,
                Response.Listener<JSONObject> { response ->
                    // Do something with the response
                    callback(response)
                },
                Response.ErrorListener { error ->
                    // Handle error
                    callback(null)
                })

        // Add the request to the RequestQueue.
        requestQueue.add(jsonObj)
    }

    fun get_content(title : String, callback: (JSONObject?) -> Unit) {
        val extension = "news"
        val params = JSONObject("{title: " + title + "}")
        // Formulate the request and handle the response.
        val jsonObj = JsonObjectRequest(Request.Method.POST, base_url+extension, params,
                Response.Listener<JSONObject> { response ->
                    // Do something with the response
                    callback(response)
                },
                Response.ErrorListener { error ->
                    // Handle error
                    callback(null)
                })

        // Add the request to the RequestQueue.
        requestQueue.add(jsonObj)
    }
}